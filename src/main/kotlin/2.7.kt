import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-14
    * NAME: Primers perfectes
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    println(checkPerfectPrime(n))
}

fun checkPerfectPrime(n: Int): Boolean {

    var sumOfDigits = 0
    for (digit in n.toString()) sumOfDigits += digit.toString().toInt()

    when (sumOfDigits) {
        0, 1 -> return false
        2, 3, 5, 7 -> return true
        else -> {
            for (i in 2..sumOfDigits/2) {
                if (sumOfDigits % i == 0) return false
            }
        }
    }
    return checkPerfectPrime(sumOfDigits)
}