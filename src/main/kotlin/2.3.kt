import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-14
    * NAME: Nombre de dígits
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    println(calculateNumberOfDigits(n))
}

fun calculateNumberOfDigits(n: Int): Int {
    return if (n == 0) 0
    else 1 + calculateNumberOfDigits(n/10)
}