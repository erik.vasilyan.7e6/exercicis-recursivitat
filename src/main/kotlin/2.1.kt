import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-14
    * NAME: Factorial
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    println(calculateFactorial(n))
}

fun calculateFactorial(n: Int): Long {
    return if (n == 0) 1
    else n * calculateFactorial(n-1)
}