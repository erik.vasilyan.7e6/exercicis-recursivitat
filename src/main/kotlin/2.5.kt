import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-14
    * NAME: Reducció de dígits
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    println(reduceDigits(n))
}

fun reduceDigits(n: Int): Int {

    var currentSumOfNums = 0
    for (digit in n.toString()) currentSumOfNums += digit.toString().toInt()

    return if (n > 9) reduceDigits(currentSumOfNums)
    else currentSumOfNums
}