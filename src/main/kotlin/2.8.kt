import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-16
    * NAME: Torres de Hanoi
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    towerOfHanoi(n)
}

fun towerOfHanoi(n: Int, from_rod: Char = 'A', to_rod: Char = 'C', aux_rod: Char = 'B') {
    if (n < 1) return
    else {
        towerOfHanoi(n - 1, from_rod, aux_rod, to_rod)
        println("$from_rod => $to_rod")
        towerOfHanoi(n - 1, aux_rod, to_rod, from_rod)
    }
}