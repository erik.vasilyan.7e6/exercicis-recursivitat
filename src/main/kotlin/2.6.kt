import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2023-01-02
    * NAME: Seqüència d’asteriscos
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    writeSequenceOfAsterisks(n)
}

fun writeSequenceOfAsterisks (n:Int) {
    var currentSequence = ""
    repeat(n) { currentSequence += '*' }

    if (n <= 1) println(currentSequence)
    else {
        writeSequenceOfAsterisks(n-1)
        println(currentSequence)
        writeSequenceOfAsterisks(n-1)
    }
}