import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-14
    * NAME: Doble factorial
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    println(calculateDoubleFactorial(n))
}

fun calculateDoubleFactorial(n: Int): Int {
    return if (n == 0) 1
    else n * calculateDoubleFactorial(n-2)
}