import java.util.*

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-14
    * NAME: Nombres creixents
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    println(isNumberGrowing(n))
}

fun isNumberGrowing(n: Int, index: Int = 0): Boolean {
    val isGrowing = n.toString().elementAt(index).toString().toInt() <= n.toString().elementAt(index + 1).toString().toInt()

    return if (index +1 == n.toString().lastIndex || !isGrowing) isGrowing
    else isNumberGrowing(n, index +1)
}